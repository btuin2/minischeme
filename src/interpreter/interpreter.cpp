/* Copyright (C) 2023 BTuin.
   This file is part of MiniScheme.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

#include "AstNode.hpp"
#include "SymbolTable.hpp"
#include "alloc.h"
#include <iostream>
#include <memory>
#include <stdexcept>

void display_expression(Gc::object_header *value) {
	if (value) {
		switch (value->type) {
		case Gc::NUMBER: {
			std::cout << get_value_number(value);
		} break;

		case Gc::STRING: {
			int size = 0;
			std::cout << get_value_string(value, &size);
		} break;

		case Gc::LIST: {
			Gc::object_header *list_element = value;
			std::cout << "(";
			bool first = true;
			while (list_element != nullptr) {
				if (!first) {
					std::cout << " ";
				}
				display_expression((get_list_data(list_element)));
				list_element = get_list_next(list_element);
				first = false;
			}
			std::cout << ")";
		} break;
		}
	} else {
		std::cout << " nil ";
	}
}

void check_type(Gc::object_header *obj, Gc::object_type type) {
	if (obj != nullptr and obj->type != type) {
		throw std::invalid_argument("Object is not a list");
	}
}

Gc::object_header *function_list(Gc::object_header *args) { return args; }

Gc::object_header *function_car(Gc::object_header *args) {
	check_type(get_list_data(args), Gc::LIST);
	return get_list_data(get_list_data(args));
}

Gc::object_header *function_cdr(Gc::object_header *args) {
	check_type(get_list_data(args), Gc::LIST);
	return get_list_next(get_list_data(args));
}

Gc::object_header *function_print(Gc::object_header *args) {
	bool first = true;
	while (args != nullptr) {
		if (!first) {
			std::cout << " ";
		} else {
			first = false;
		}
		display_expression(get_list_data(args));
		args = get_list_next(args);
	}
	std::cout << std::endl;
	return nullptr;
}

int get_list_size(Gc::object_header *list) {
	if (list == nullptr) {
		return 0;
	}

	if (list->type != Gc::LIST) {
		throw std::invalid_argument("object is not a list");
	}

	int size = 0;
	while (list != nullptr) {
		size++;
		list = get_list_next(list);
	}
	return size;
}

Gc::object_header *function_setf(Gc::object_header *args) {

	int arg_number = get_list_size(args);
	if (arg_number < 2 || arg_number % 2 != 0) {
		throw std::invalid_argument("wrong number of argument for setf");
	}

	Gc::object_header *x = nullptr, *y = nullptr;

	while (args != nullptr) {
		x = get_list_data(args);
		args = get_list_next(args);
		y = get_list_data(args);
		args = get_list_next(args);

		replace_object_value(x, y);
	}
	return y;
}

void add_native_functions(std::shared_ptr<SymbolTable> table) {
	table->add_function("list", function_list);
	table->add_function("car", function_car);
	table->add_function("cdr", function_cdr);
	table->add_function("print", function_print);
	// table->add_function("setf", function_setf);
}

Gc::object_header *call_function(std::string identifier,
                                 Gc::object_header *argument_list,
                                 std::shared_ptr<SymbolTable> sym_table) {
	return sym_table->get_function(identifier)(argument_list);
}
