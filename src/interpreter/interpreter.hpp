#include "alloc.h"

#include "SymbolTable.hpp"
#include "alloc.h"
#include "ast.hpp"

#include <cassert>
#include <iostream>
#include <memory>
#include <stack>
#include <stdexcept>
#include <variant>

void display_expression(Gc::object_header *value);
void add_native_functions(std::shared_ptr<SymbolTable> table);
Gc::object_header *call_function(std::string identifier,
                                 Gc::object_header *argument_list,
                                 std::shared_ptr<SymbolTable> sym_table);

class AstNodeInterpreterVisitor : Ast::AstNodeVisitor {

  private:
	std::shared_ptr<SymbolTable> sym_table;

  public:
	AstNodeInterpreterVisitor(std::shared_ptr<SymbolTable> sym_table)
	    : sym_table(sym_table) {
		add_native_functions(sym_table);
	}

	void visit(Ast::Program *prog) override {
		for (const auto &expr : prog->expressions) {
			expr->accept(this);
			prog->object_value = expr->object_value;
		}
	}

	void visit(Ast::Expr *expr) override {
		for (const auto &child : expr->children) {
			child->accept(this);
			expr->object_value = child->object_value;
		}
	}

	void visit(Ast::Call *call) override {
		std::vector<Gc::object_header *> list_values;

		for (const auto &element : call->listContent) {
			element->accept(this);
			list_values.push_back(element->object_value);
		}

		Gc::object_header *arguments =
		    Gc::alloc_list(list_values.data(), list_values.size());

		call->object_value =
		    call_function(call->identifier, arguments, this->sym_table);
	}

	void visit(Ast::Atom *atom) override {

		struct VisitAtom {
			Gc::object_header *operator()(double d) {
				return Gc::alloc_object(&d, Gc::NUMBER, sizeof(d));
			}
			Gc::object_header *operator()(std::string &s) {
				return Gc::alloc_object((void *)s.c_str(), Gc::STRING,
				                        s.size() + 1);
			}
			Gc::object_header *operator()(std::monostate) { return nullptr; }
		};

		switch (atom->type) {
		case Ast::NUMBER:
			atom->object_value = std::visit(VisitAtom(), atom->value);
			break;
		case Ast::STRING:
			atom->object_value = std::visit(VisitAtom(), atom->value);
			break;
		case Ast::IDENTIFIER:
			atom->object_value = (this->sym_table->get_value(
			    std::get<std::string>(atom->value)));
			break;
		case Ast::NIL:
			atom->object_value = std::visit(VisitAtom(), atom->value);
			break;
		}
	}

	void visit(Ast::Let *let) override {

		sym_table->push_env(false);

		let->syntax_bindings->accept(this);
		if (let->body) {
			let->body->accept(this);
			let->object_value = let->body->object_value;
		}

		sym_table->pop_env();
	}

	void visit(Ast::SyntaxBindings *sbs) override {
		for (auto &sb : sbs->syntax_bindings) {
			sb->accept(this);
		}
	}

	void visit(Ast::SyntaxBinding *sb) override {
		sb->value->accept(this);
		sym_table->define_local_variable(sb->identifier,
		                                 sb->value->object_value);
	}

	void visit(Ast::If *i) override {
		i->condition->accept(this);
		if (i->condition->object_value != nullptr) {
			i->if_true->accept(this);
			i->object_value = i->if_true->object_value;
		} else {
			if (i->if_false) {
				i->if_false->accept(this);
				i->object_value = i->if_false->object_value;
			} else {
				i->object_value = nullptr;
			}
		}
	}

	void visit(Ast::Setq * setq) override {
		for (auto & elem : setq->assignments) {
			std::get<1>(elem)->accept(this);
			sym_table->set_value(std::get<0>(elem), std::get<1>(elem)->object_value);
			setq->object_value = std::get<1>(elem)->object_value;
		}
	}

	void visit(Ast::While * w) override {
		w->condition->accept(this);
		while (w->condition->object_value != nullptr) {
			w->body->accept(this);
			w->object_value = w->body->object_value;
			w->condition->accept(this);
		}
	}
};
