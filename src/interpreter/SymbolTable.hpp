#pragma once

/* Copyright (C) 2023 BTuin.
   This file is part of MiniScheme.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

#include "AstNode.hpp"
#include "alloc.h"
#include "ast.hpp"

#include <functional>
#include <optional>
#include <stack>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <vector>

class SymbolTable;

typedef std::function<Gc::object_header *(Gc::object_header *)> function_type;

class Environnement {
	Environnement *enclosing = nullptr;

  public:
	std::unordered_map<std::string, Gc::object_header *> env;

	void define(std::string name, Gc::object_header *value) {
		env[name] = value;
	}

	std::optional<Gc::object_header *> get(std::string name) {
		if (!this->env.empty()) {
			return env.at(name);
		} else {
			return {};
		}
	}

	bool contains(std::string variable) { return env.contains(variable); }

	void set_enclosing(Environnement *enclosing) {
		this->enclosing = enclosing;
	}

	Environnement *get_enclosing() { return this->enclosing; }

	Environnement(){};
	Environnement(Environnement *enclosing) { this->enclosing = enclosing; }
};

class SymbolTable {

  private:
	Environnement global;
	std::stack<std::unique_ptr<Environnement>> envs;

	std::unordered_map<std::string, function_type> functions;

  public:
	void push_env(bool new_context) {
		auto new_env = std::make_unique<Environnement>();
		if (!envs.empty() && !new_context) {
			new_env->set_enclosing(envs.top().get());
		}
		envs.push(std::move(new_env));
	}

	void pop_env() {
		if (!envs.empty()) {
			envs.pop();
		}
	}

	void define_global_variable(const std::string &name,
	                            Gc::object_header *value) {
		global.define(name, value);
	}

	void define_local_variable(const std::string &name,
	                           Gc::object_header *value) {
		if (!envs.empty()) {
			envs.top()->define(name, value);
		} else {
			throw std::out_of_range("No local environment to set variable \"" +
			                        name + "\"");
		}
	}

	void set_value(const std::string &name, Gc::object_header *value) {
		if (!envs.empty()) {
			Environnement *env = envs.top().get();
			while (env != nullptr) {
				if (env->contains(name)) {
					env->define(name, value);
					return;
				}
				env = env->get_enclosing();
			}
		} else {
			if (global.contains(name)) {
				global.define(name, value);
			} else {
				throw std::invalid_argument("variable \"" + name +
				                            "\" is not defined");
			}
		}
	}

	Gc::object_header *get_value(const std::string &name) {
		if (!envs.empty()) {
			Environnement *env = envs.top().get();
			while (env != nullptr) {
				auto res = env->get(name);
				if (res) {
					return res.value();
				} else {
					env = env->get_enclosing();
				}
			}
		}
		auto res = global.get(name);
		if (res) {
			return res.value();
		}
		throw std::out_of_range("Variable \"" + name + "\" is not defined");
	}

	void add_function(std::string name, function_type fn) {
		this->functions[name] = fn;
	}

	function_type get_function(std::string name) {
		if (!this->functions.contains(name)) {
			throw std::invalid_argument("Function is not defined: " + name);
		}
		return this->functions.at(name);
	}
};
