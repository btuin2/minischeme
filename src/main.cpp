/* 2022 BTuin

   This file is part of MiniScheme

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */


#include "SymbolTable.hpp"
#include "driver.hpp"
#include "interpreter.hpp"

#include <exception>
#include <memory>
#include <optional>

Interpreter::Error run(driver &drv, std::optional<std::string> file,
					   std::shared_ptr<SymbolTable> sym_table) {
	auto parse = drv.parse(file);
	if (std::get<1>(parse) != Interpreter::ok)
		return std::get<1>(parse);

	AstNodeInterpreterVisitor visitor(sym_table);
	Ast::Program *prog = std::get<0>(parse).get();
	visitor.visit(prog);

	display_expression(prog->object_value);
	std::cout << std::endl;
	return std::get<1>(parse);
}

int main(int argc, char *argv[]) {
	int res = 0;
	driver drv;
	bool file_used = false;
	std::shared_ptr<SymbolTable> sym_table = std::make_shared<SymbolTable>();

	for (int i = 1; (i < argc && res == 0); ++i)
		if (argv[i] == std::string("-p"))
			drv.trace_parsing = true;
		else if (argv[i] == std::string("-l"))
			drv.trace_scanning = true;
		else {
			file_used = true;
			res = run(drv, std::string(argv[i]), sym_table);
		}
	if (!file_used) {
		while (res != Interpreter::eof && res != Interpreter::unknown) {
			try {
				res = run(drv, {}, sym_table);
			} catch (std::exception &e) {
				std::cerr << "Error: " << e.what() << std::endl;
			}
		}
	}
	return res;
}
