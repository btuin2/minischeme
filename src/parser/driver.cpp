/* Copyright (C) 2005-2015, 2018-2021 Free Software Foundation, Inc.
   2022 BTuin

   This file is part of MiniScheme

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

#include "driver.hpp"
#include "scanner.hpp"

#include <cstring>
#include <fstream>
#include <iostream>
#include <istream>
#include <memory>
#include <sstream>
#include <tuple>

#include <readline/history.h>
#include <readline/readline.h>

driver::driver() : trace_parsing(false), trace_scanning(false) {}

driver::~driver() {
	delete this->ps;
	delete this->sc;
}

using namespace Interpreter;

std::tuple<std::unique_ptr<Ast::Program>, Error>
driver::parse(std::optional<std::string> f) {
	if (f.has_value()) {
		return this->parse(f.value());
	} else {
		return this->parse();
	}
}

std::tuple<std::unique_ptr<Ast::Program>, Error>
driver::parse(std::istream &is) {
	return parse_helper(is);
}

std::tuple<std::unique_ptr<Ast::Program>, Error>
driver::parse(const std::string &f) {
	std::ifstream is(f);
	return parse_helper(is);
}

std::tuple<std::unique_ptr<Ast::Program>, Error> driver::parse() {
	char *raw_input = readline("> ");
	if (raw_input != nullptr) {
		std::string input(raw_input);
		if (input.size() > 0) {
			add_history(raw_input);
		}
		std::stringstream ss(input);
		std::free(raw_input);
		return parse_helper(ss);
	} else {
		return {nullptr, Interpreter::eof};
	}
}

std::tuple<std::unique_ptr<Ast::Program>, Error>
driver::parse_helper(std::istream &is) {
	delete (sc);
	this->sc = new scanner(&is, this->depth, this->trace_scanning);
	delete (ps);

	std::unique_ptr<Ast::Program> ast;
	this->ps = new yy::parser(*this, &ast, *sc);

	this->ps->set_debug_level(trace_parsing);
	int res = this->ps->parse();
	Error err = Interpreter::unknown;
	if (res == 0) {
		err = Interpreter::ok;
	} else {
		err = Interpreter::parser;
	}

	return {std::move(ast), err};
}
