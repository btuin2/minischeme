/* Copyright (C)
   2020 Jonathan Beard
   2023 BTuin.
   This file is part of MiniScheme.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

#pragma once


#include <cstddef>
#include <memory>
#if ! defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#include "location.hh"
#include "parser.hpp"

#include <istream>


class scanner : public yyFlexLexer{
public:

	scanner(std::istream *in, std::shared_ptr<size_t> depth, bool trace_scanning) : yyFlexLexer(in), depth(depth)
	{
		loc = new yy::parser::location_type();
		this->set_debug(trace_scanning);
	}

	virtual ~scanner() { delete loc; }

	//get rid of override virtual function warning
	using FlexLexer::yylex;
	int yylex(yy::parser::semantic_type * const lval,
					  yy::parser::location_type *location);

private:
	yy::parser::semantic_type *yylval = nullptr;
	yy::parser::location_type *loc    = nullptr;
	std::shared_ptr<size_t> depth;
};
