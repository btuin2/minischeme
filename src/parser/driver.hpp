/* Copyright (C) 2023 BTuin.
   This file is part of MiniScheme.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

#pragma once

#include "parser.hpp"
#include "scanner.hpp"

#include "ast.hpp"
#include <cstddef>
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <tuple>

namespace Interpreter {
enum Error { ok, parser, runtime, eof, unknown };
}

class driver {
  public:
	driver();

	~driver();

	std::tuple<std::unique_ptr<Ast::Program>, Interpreter::Error>
	parse(std::optional<std::string> f);
	std::tuple<std::unique_ptr<Ast::Program>, Interpreter::Error>
	parse(const std::string &f);
	std::tuple<std::unique_ptr<Ast::Program>, Interpreter::Error>
	parse(std::istream &iss);
	std::tuple<std::unique_ptr<Ast::Program>, Interpreter::Error> parse();

	// Whether to generate parser debug traces.
	bool trace_parsing;
	// Whether to generate scanner debug traces.
	bool trace_scanning;

	void increaseScope() { (*depth)++; }
	void decreateScope() { (*depth)--; }

  private:
	std::tuple<std::unique_ptr<Ast::Program>, Interpreter::Error>
	parse_helper(std::istream &is);
	scanner *sc = nullptr;
	yy::parser *ps = nullptr;

	std::string file;

	// Depth of the scope
	std::shared_ptr<size_t> depth = std::make_shared<size_t>(0);

	// The token's location used by the scanner.
	yy::location location;
};
