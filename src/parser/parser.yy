/*  Copyright (C) 2005-2015, 2018-2021 Free Software Foundation, Inc.
    2023 BTuin

   This file is part of MiniScheme.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */


%skeleton "lalr1.cc"
%require "3.8"
%language "c++"
%header

%define api.value.type variant
%define parse.assert

%code requires {
  # include <string>
  #include "ast.hpp"
  class driver;
  class scanner;
}

// The parsing context.
%parse-param { driver &drv }
%parse-param { std::unique_ptr<Ast::Program> *ast }
%parse-param { scanner &sc }

%locations

%define parse.trace
%define parse.error detailed
%define parse.lac full

%code {
    #include <iostream>
    #include <cstdlib>
    #include <fstream>

    #include "driver.hpp"


#undef yylex
#define yylex sc.yylex

}


%printer { yyo << $$; } <*>;


%token <double> NUMBER
%token <std::string> IDENTIFIER
%token OP
%token CP
%token DOT
%token LET
%token IF
%token SETQ
%token NIL
%token WHILE
%token <std::string> STRING

%nterm <std::unique_ptr<Ast::Program>> program
%nterm <std::unique_ptr<Ast::Expr>> s_expression
%nterm <std::unique_ptr<Ast::Expr>> body
%nterm <std::unique_ptr<Ast::Atom>> atom
%nterm <std::unique_ptr<Ast::Call>> call
%nterm <std::unique_ptr<Ast::Let>> let
%nterm <std::unique_ptr<Ast::Setq>> setq
%nterm <std::unique_ptr<Ast::If>> if
%nterm <std::unique_ptr<Ast::While>> while
%nterm <std::unique_ptr<Ast::SyntaxBindings>> syntax_bindings
%nterm <std::unique_ptr<Ast::SyntaxBinding>> syntax_binding

%%

top_level:
    program { *ast = std::move($1); }
  ;

program:
    program s_expression
	    {
	      $$ = std::move($1);
	      $$->addExpr(std::move($2));
	    }
  | s_expression
	    {
	      $$ = std::make_unique<Ast::Program>();
	      $$->addExpr(std::move($1));
	    }
  ;

s_expression:
    atom
	    { $$ = std::make_unique<Ast::Expr>();
	      $$->addChildren(std::move($1));
	    }
  | OP call  CP
	    {
	      $$ = std::make_unique<Ast::Expr>();
	      $$->addChildren(std::move($2));
	    }
  | let
	    {
	      $$ = std::make_unique<Ast::Expr>();
	      $$->addChildren(std::move($1));
	    }
  | if
        {
	      $$ = std::make_unique<Ast::Expr>();
	      $$->addChildren(std::move($1)); 
        }
  | OP setq CP
        {
	      $$ = std::make_unique<Ast::Expr>();
	      $$->addChildren(std::move($2)); 
        }
   | while
        {
	      $$ = std::make_unique<Ast::Expr>();
	      $$->addChildren(std::move($1)); 
        }
  ;

let:
    OP LET OP syntax_bindings CP body CP
	    {
	      $$ = std::make_unique<Ast::Let>(std::move($4),
					      std::move($6));
	    }
  | OP LET OP syntax_bindings CP CP
	    {
	      $$ = std::make_unique<Ast::Let>(std::move($4),
					      nullptr);
	    }
  ;

body:
  s_expression
        {
		  $$ = std::make_unique<Ast::Expr>();
		  $$->addChildren(std::move($1));
        }
   | body s_expression
	    {
		  $$ = std::move($1);
		  $$->addChildren(std::move($2));
        }
  ;

syntax_bindings:
    syntax_bindings syntax_binding
	    {
	      $1->addSyntaxBinding(std::move($2));
	      $$ = std::move($1);
	    }
  | syntax_binding
	    {
	      $$ = std::make_unique<Ast::SyntaxBindings>();
	      $$->addSyntaxBinding(std::move($1));
	    }
  ;

syntax_binding:
    OP IDENTIFIER s_expression CP
	    {
	      $$ = std::make_unique<Ast::SyntaxBinding>(std::move($2), std::move($3));
	    }
  ;

setq:
    SETQ IDENTIFIER s_expression
	    {
          $$ = std::make_unique<Ast::Setq>();
		  $$->addAssignement($2, std::move($3));
        }
  | setq IDENTIFIER s_expression
        {
          $$ = std::move($1);
		  $$->addAssignement($2, std::move($3));
        }
  ;

if:
	OP IF s_expression s_expression s_expression CP
	    {
	      $$ = std::make_unique<Ast::If>(std::move($3), std::move($4), std::move($5));
		}
  | OP IF s_expression s_expression CP
        {
		  $$ = std::make_unique<Ast::If>(std::move($3), std::move($4), nullptr);
        }
  ;


while:
	OP WHILE s_expression body CP
		  {
			$$ = std::make_unique<Ast::While>(std::move($3), std::move($4));
		  }
  ;

call:
    IDENTIFIER
	    {
	      $$ = std::make_unique<Ast::Call>();
	      $$->identifier = $1;
	    }
  | call s_expression
	    {
	      $1->Add(std::move($2));
	      $$ = std::move($1);
	    }
  ;


atom:

    NUMBER
	    {
	      $$ = std::make_unique<Ast::Atom>(Ast::NUMBER, $1);
	    }
  | IDENTIFIER
	    {
	      $$ = std::make_unique<Ast::Atom>(Ast::IDENTIFIER, $1);
	    }
  | STRING
	    {
	      $$ = std::make_unique<Ast::Atom>(Ast::STRING, $1);
	    }
  | OP CP
       {
  	     $$ = std::make_unique<Ast::Atom>(Ast::NIL, std::monostate{});
       }
  | NIL
       {
  	     $$ = std::make_unique<Ast::Atom>(Ast::NIL, std::monostate{});
       }
  ;
%%

void
yy::parser::error (const location_type& l, const std::string& m)
{
  std::cerr << l << ": " << m << '\n';
}
