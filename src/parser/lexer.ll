/* Copyright (C) 2005-2015, 2018-2021 Free Software Foundation, Inc.
   2023 BTuin

   This file is part of MiniScheme.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

%{
#include <string>
#include "scanner.hpp"

#undef  YY_DECL
#define YY_DECL int scanner::yylex(yy::parser::semantic_type * const lval, yy::parser::location_type *loc)

using token = yy::parser::token;

#define yyterminate() return(token::YYEOF)

#define YY_USER_ACTION loc->step(); loc->columns(yyleng);

%}

%option debug
%option nodefault
/* Necessary so Bison will generate a yyFlexLexer::yylex method, so
   we can override it in the scanner. */
%option yyclass="scanner"
%option noyywrap
%option c++

blank          [ \t\r]
nil            nil
number         [0-9]+(\.[0-9]+)?
identifier      [-a-zA-Z*+/][-a-zA-Z0-9*+/]*
op              \(
cp              \)
dot             "."
string          \"([^\\\"]|\\.)*\"
comment         ^;;.*

%%
%{
	yylval = lval;
%}

{blank}+     { }
{comment}     { }
\n+        { loc->lines(); }

{nil}           { return token::NIL; }
{number}        {
				yylval->build<double>(std::stod(yytext));
				return token::NUMBER;
				}
"if"            { return token::IF; }
"setq"          { return token::SETQ; }
"let"           { return token::LET; }
"while"         { return token::WHILE; }
{identifier}    {
				yylval->build<std::string>(yytext);
				return token::IDENTIFIER; }
{op}            { return token::OP; }
{cp}            { return token::CP; }
{dot}           { return token::DOT; }
{string}        {
				yylval->build<std::string>(yytext);
				return token::STRING;
				}

.               { throw yy::parser::syntax_error (*loc, "invalid character: " + std::string(yytext)); }
<<EOF>>    return token::YYEOF;
%%
