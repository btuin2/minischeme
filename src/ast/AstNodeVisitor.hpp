#pragma once

/* Copyright (C) 2023 BTuin.
   This file is part of MiniScheme.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

#include "ast.hpp"
namespace Ast {
class Program;
class Expr;
class Call;
class Atom;
class Let;
class SyntaxBindings;
class SyntaxBinding;
class Setq;
class If;
class While;

class AstNodeVisitor {
  public:
	virtual void visit(Program *) = 0;
	virtual void visit(Expr *) = 0;
	virtual void visit(Call *) = 0;
	virtual void visit(Atom *) = 0;
	virtual void visit(SyntaxBindings *) = 0;
	virtual void visit(SyntaxBinding *) = 0;
	virtual void visit(Let *) = 0;
	virtual void visit(Setq *) = 0;
	virtual void visit(If *) = 0;
	virtual void visit(While *) = 0;
};
} // namespace Ast
