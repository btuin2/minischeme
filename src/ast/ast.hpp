#pragma once

/* Copyright (C) 2023 BTuin.
   This file is part of MiniScheme.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

#include "AstNode.hpp"
#include "AstNodeVisitor.hpp"

#include <algorithm>
#include <iostream>
#include <memory>
#include <string>
#include <type_traits>
#include <variant>
#include <vector>

namespace Ast {

enum Type { NUMBER, STRING, IDENTIFIER, NIL };

class Program : public AstNode {
  public:
	std::vector<std::unique_ptr<Expr>> expressions;

	void addExpr(std::unique_ptr<Expr> expr) {
		expressions.push_back(std::move(expr));
	}

	void accept(AstNodeVisitor *visitor) override { visitor->visit(this); }
};

class Expr : public AstNode {
  public:
	std::vector<std::unique_ptr<AstNode>> children{};
	void addChildren(std::unique_ptr<AstNode> c) {
		this->children.push_back(std::move(c));
	}

	void accept(AstNodeVisitor *visitor) override { visitor->visit(this); }
};

class Call : public AstNode {

  public:
	std::string identifier;
	std::vector<std::unique_ptr<AstNode>> listContent{};
	void Add(std::unique_ptr<AstNode> node) {
		listContent.push_back(std::move(node));
	}

	void accept(AstNodeVisitor *visitor) override { visitor->visit(this); }
};

class Atom : public AstNode {
  public:
	Type type;
	std::variant<std::monostate, double, std::string> value;
	Atom(Type type, std::variant<std::monostate, double, std::string> value) {
		this->type = type;
		this->value = value;
	}

	void accept(AstNodeVisitor *visitor) override { visitor->visit(this); }
};

class SyntaxBinding : public AstNode {
  public:
	std::string identifier;
	std::unique_ptr<Expr> value;

	SyntaxBinding(std::string identifier, std::unique_ptr<Expr> value) {
		this->identifier = identifier;
		this->value = std::move(value);
	}

	void accept(AstNodeVisitor *visitor) override { visitor->visit(this); }
};

class SyntaxBindings : public AstNode {

  public:
	std::vector<std::unique_ptr<SyntaxBinding>> syntax_bindings{};
	void addSyntaxBinding(std::unique_ptr<SyntaxBinding> syntax_binding) {
		syntax_bindings.push_back(std::move(syntax_binding));
	}

	void accept(AstNodeVisitor *visitor) override { visitor->visit(this); }
};

class Let : public AstNode {

  public:
	std::unique_ptr<SyntaxBindings> syntax_bindings;
	std::unique_ptr<Expr> body;
	Let(std::unique_ptr<SyntaxBindings> syntax_bindings,
	    std::unique_ptr<Expr> body) {
		this->syntax_bindings = std::move(syntax_bindings);
		this->body = std::move(body);
	}

	void accept(AstNodeVisitor *visitor) override { visitor->visit(this); }
};

class Setq : public AstNode {

  public:
	std::vector<std::tuple<std::string, std::unique_ptr<Expr>>> assignments;

	void addAssignement(std::string identifier, std::unique_ptr<Expr> value) {
		assignments.emplace_back(identifier, std::move(value));
	}

	void accept(AstNodeVisitor *visitor) override { visitor->visit(this); }
};

class If : public AstNode {
  public:
	std::unique_ptr<Expr> condition;
	std::unique_ptr<Expr> if_true;
	std::unique_ptr<Expr> if_false;

	If(std::unique_ptr<Expr> condition, std::unique_ptr<Expr> if_true,
	   std::unique_ptr<Expr> if_false) {
		this->condition = std::move(condition);
		this->if_true = std::move(if_true);
		this->if_false = std::move(if_false);
	}

	void accept(AstNodeVisitor *visitor) override { visitor->visit(this); }
};

class While : public AstNode {
  public:
	std::unique_ptr<Expr> condition;
	std::unique_ptr<Expr> body;

	While(std::unique_ptr<Expr> condition, std::unique_ptr<Expr> body) {
		this->condition = std::move(condition);
		this->body = std::move(body);
	}

	void accept(AstNodeVisitor *visitor) override { visitor->visit(this); }
};

} // namespace Ast
