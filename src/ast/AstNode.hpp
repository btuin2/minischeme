#pragma once

/* Copyright (C) 2023 BTuin.
   This file is part of MiniScheme.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

#include "alloc.h"


#include <memory>
#include <ostream>
#include <vector>

namespace Ast {

class AstNodeVisitor;

class AstNode {

public:
	virtual void accept(AstNodeVisitor *v) = 0;

	Gc::object_header* object_value = nullptr;

	friend std::ostream &operator<<(std::ostream &os, const Ast::AstNode &) {
		os << "Default print for AstNode";
		return os;
	}

	virtual ~AstNode() = default;
};
} // namespace Ast
