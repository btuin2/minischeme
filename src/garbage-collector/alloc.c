/* Copyright (C) 2023 BTuin.
   This file is part of MiniScheme.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

#include "alloc.h"

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <sys/queue.h>

#define MAX_CLASS_SIZE 5

struct memory_block {
	void *data;
	size_t size;
};

typedef struct block {
	struct block *next;
	size_t size;
	size_t used;
	void *data;
} block;

struct object_header *alloc_object(void *value, enum object_type type,
                                   int size) {
	struct object_header *header_ptr = malloc(sizeof(struct object_header));
	void *object_ptr = malloc(size);

	struct object_header new_header;
	new_header.size = size;
	new_header.magic_bytes = 0xdeadbeef;
	new_header.type = type;
	new_header.object = object_ptr;

	memcpy(object_ptr, value, size);

	*header_ptr = new_header;

	return header_ptr;
}

void replace_object_value(struct object_header *dest,
                          struct object_header *src) {
	assert(dest->magic_bytes == magic_value);
	assert(src->magic_bytes == magic_value);

	*dest = *src;
}

struct object_header *alloc_number(double value) {
	return alloc_object(&value, NUMBER, sizeof(value));
}

struct object_header *alloc_string(const char *str, int size) {
	return alloc_object((void *)str, STRING, size + 1);
}

double get_value_number(struct object_header *ptr) {
	assert(ptr->type == NUMBER);
	assert(ptr->magic_bytes == magic_value);
	return *(double *)ptr->object;
}

char *get_value_string(struct object_header *ptr, int *size) {
	assert(ptr->type == STRING);
	assert(ptr->magic_bytes == magic_value);
	*size = ptr->size - 1;
	return (char *)ptr->object;
}

struct list_element create_list_element(struct object_header *data) {
	struct list_element element_value;
	element_value.data = data;
	element_value.next = NULL;

	return element_value;
}

struct list_element *get_list_element(struct object_header *ptr) {
	assert(ptr->type == LIST);
	assert(ptr->magic_bytes == magic_value);
	return (struct list_element *)ptr->object;
}

struct object_header *get_list_data(struct object_header *ptr) {
	if (ptr == NULL) {
		return NULL;
	}
	assert(ptr->type == LIST);
	assert(ptr->magic_bytes == magic_value);
	return get_list_element(ptr)->data;
}

struct object_header *alloc_list(struct object_header **value, int n) {
	if (n == 0) {
		return NULL;
	}

	struct list_element new_list_element = create_list_element(*value);

	struct object_header *current_header =
	    alloc_object(&new_list_element, LIST, sizeof(struct list_element));

	struct object_header *first_header = current_header;

	for (int i = 1; i < n; i++) {
		value++;
		new_list_element = create_list_element(*value);
		struct object_header *new_header =
		    alloc_object(&new_list_element, LIST, sizeof(struct list_element));
		get_list_element(current_header)->next = new_header;
		current_header = new_header;
	}
	return first_header;
}

struct object_header *get_list_next(struct object_header *ptr) {
	if (ptr == NULL) {
		return NULL;
	}
	struct list_element *elem = get_list_element(ptr);
	return elem->next;
}

block alloc_block(size_t s) {
	block b;
	b.next = NULL;
	b.size = s;
	b.data = malloc(s);

	return b;
}
