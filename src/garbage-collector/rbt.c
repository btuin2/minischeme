#include "rbt.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

struct mem_node mem_nil = {.color = MEM_BLACK,
                           .left = &mem_nil,
                           .right = &mem_nil,
                           .parent = &mem_nil,
                           .value = NULL};

mem_node *create_mem_node(void *value) {
	mem_node *node = malloc(sizeof(mem_node));
	node->color = MEM_BLACK;
	node->left = &mem_nil;
	node->right = &mem_nil;
	node->parent = &mem_nil;
	node->value = value;

	return node;
}

rbtree *create_rbtree() {
	rbtree *tree = malloc(sizeof(rbtree));
	tree->root = &mem_nil;
	return tree;
}

const mem_node* get_mem_nil_value() {
	return &mem_nil;
}

mem_node *find_node(rbtree *tree, void *value) {
	mem_node *node = tree->root;
	while (node != &mem_nil)
		if ((block *)value < (block *)node->value) {
			node = node->left;
		} else if ((block *)value > (block *)node->value) {
			node = node->right;
		} else {
			break;
		}

	assert(node->value == value);

	return node;
}

void *find_block(rbtree *tree, void *value) {
	mem_node *node = tree->root;
	while (node != &mem_nil && !((block *)value >= (block *)node->value &&
	                             (block *)value < ((block *)node->value) + 1))
		if ((block *)value < (block *)node->value) {
			node = node->left;
		} else if ((block *)value > (block *)node->value) {
			node = node->right;
		} else {
			break;
		}

	assert((block *)value >= (block *)node->value &&
	       (block *)value < ((block *)node->value) + 1);

	return node->value;
}

void rotate_left(rbtree *tree, mem_node *x) {
	mem_node *y = x->right;

	x->right = y->left;

	if (y->left != &mem_nil) {
		y->left->parent = x;
	}

	y->parent = x->parent;

	if (x->parent == &mem_nil) {
		tree->root = y;
	} else if (x == x->parent->left) {
		x->parent->left = y;
	} else {
		x->parent->right = y;
	}

	y->left = x;
	x->parent = y;
}

void rotate_right(rbtree *tree, mem_node *x) {
	mem_node *y = x->left;

	x->left = y->right;

	if (y->right != &mem_nil) {
		y->right->parent = x;
	}

	y->parent = x->parent;

	if (x->parent == &mem_nil) {
		tree->root = y;
	} else if (x == x->parent->right) {
		x->parent->right = y;
	} else {
		x->parent->left = y;
	}

	y->right = x;
	x->parent = y;
}

void fix_insert(rbtree *tree, mem_node *z) {
	while (z->parent->color == MEM_RED) {
		if (z->parent == z->parent->parent->left) {
			mem_node *y = z->parent->parent->right;
			if (y->color == MEM_RED) {
				z->parent->color = MEM_BLACK;
				y->color = MEM_BLACK;
				z->parent->parent->color = MEM_RED;
				z = z->parent->parent;
			} else {
				if (z == z->parent->right) {
					z = z->parent;
					rotate_left(tree, z);
				}
				z->parent->color = MEM_BLACK;
				z->parent->parent->color = MEM_RED;
				rotate_right(tree, z->parent->parent);
			}
		} else {
			mem_node *y = z->parent->parent->left;
			if (y->color == MEM_RED) {
				z->parent->color = MEM_BLACK;
				y->color = MEM_BLACK;
				z->parent->parent->color = MEM_RED;
				z = z->parent->parent;
			} else {
				if (z == z->parent->left) {
					z = z->parent;
					rotate_right(tree, z);
				}
				z->parent->color = MEM_BLACK;
				z->parent->parent->color = MEM_RED;
				rotate_left(tree, z->parent->parent);
			}
		}
	}
	tree->root->color = MEM_BLACK;
}

void insert(rbtree *tree, mem_node *z) {

	mem_node *y = &mem_nil;
	mem_node *x = tree->root;

	while (x != &mem_nil) {
		y = x;
		if (z->value < x->value) {
			x = x->left;
		} else {
			x = x->right;
		}
	}
	z->parent = y;
	if (y == &mem_nil) {
		tree->root = z;
	} else if (z->value < y->value) {
		y->left = z;
	} else {
		y->right = z;
	}

	z->left = &mem_nil;
	z->right = &mem_nil;
	z->color = MEM_RED;

	fix_insert(tree, z);
}

void insert_value(rbtree *tree, void *value) {
	insert(tree, create_mem_node(value));
}
