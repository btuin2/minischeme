/* Copyright (C) 2023 BTuin. -*- mode: C -*-
   This file is part of MiniScheme.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

#pragma once

#include <stdint.h>
#ifdef __cplusplus
namespace Gc {
extern "C" {
#endif

const int64_t magic_value = 0xdeadbeef;

enum types {
	SCALAR,
	ONE_BYTE_ARRAY,
	TWO_BYTES_ARRAY,
	ARRAY_OF_REFERENCES,
	ARRAY_OF_EIGHT_BYTES
};

enum object_type { NUMBER, STRING, LIST };

struct object_header {
	int64_t magic_bytes;
	enum object_type type;
	int size;
	void *object;
};

struct list_element {
	struct object_header *next;
	struct object_header *data;
};

struct object_header *alloc_object(void *value, enum object_type type,
                                   int size);
struct object_header *alloc_number(double value);
struct object_header *alloc_string(const char *str, int size);
struct object_header *alloc_list(struct object_header **value, int n);

void replace_object_value(struct object_header *dest,
                          struct object_header *src);

double get_value_number(struct object_header *ptr);
char *get_value_string(struct object_header *ptr, int *size);
struct object_header *get_list_next(struct object_header *ptr);
struct object_header *get_list_data(struct object_header *ptr);

void *myallocint();

#ifdef __cplusplus
}
}
#endif
