#pragma once

#include "alloc.h"

#ifdef __cplusplus
namespace Gc {
extern "C" {
#endif

typedef struct mem_node {
	enum color { MEM_RED, MEM_BLACK } color;
	struct mem_node *left, *right, *parent;
	struct block *value;
} mem_node;

typedef struct rbtree {
	mem_node *root;
} rbtree;

rbtree *create_rbtree();

void *find_block(rbtree *tree, void *value);
void insert_value(rbtree *tree, void *value);
mem_node *find_node(rbtree *tree, void *value);

/* Only used for tests! */
const mem_node* get_mem_nil_value();

#ifdef __cplusplus
}
}
#endif
