#include "rbt.h"

#include <algorithm>
#include <cstddef>
#include <cstdlib>
#include <gtest/gtest.h>
#include <list>
#include <stdint.h>


using namespace Gc;


TEST(rbtree, simple) {
	rbtree *tree = create_rbtree();

	intptr_t values[] = {2, 1, 4, 5, 9, 3, 6, 7};

	for (size_t i = 0; i < (sizeof values) / sizeof(intptr_t); i++) {
		insert_value(tree, (void *)values[i]);
	}

	for (size_t i = 0; i < (sizeof values) / sizeof(intptr_t); i++) {
		EXPECT_EQ((void *)values[i], find_node(tree, (void *)values[i])->value);
	}
}




TEST(rbtree, insert) {
	rbtree *tree = create_rbtree();

	intptr_t values[] = {3,  7,  10, 12, 14, 15, 16, 17, 19, 20,
	                     21, 23, 26, 28, 30, 35, 38, 39, 41, 47};

	for (size_t i = 0; i < (sizeof values) / sizeof(intptr_t); i++) {
		insert_value(tree, (void *)values[i]);
	}

	for (size_t i = 0; i < (sizeof values) / sizeof(intptr_t); i++) {
		EXPECT_EQ((void *)values[i], find_node(tree, (void *)values[i])->value);
	}
}


TEST(rbtree, insertMany) {
	rbtree *tree = create_rbtree();

	for (size_t i = 0; i < 500000; i++) {
		insert_value(tree, (void *)i);
	}

	for (size_t i = 0; i < 500000; i++) {
		EXPECT_EQ((void *)i, find_node(tree, (void *)i)->value);
	}
}

TEST(rbtree, doubleInsert) {
	rbtree *tree = create_rbtree();

	std::srand(1);

	for (size_t i = 0; i < 500000; i++) {
		insert_value(tree, (void *)i);
	}

	for (size_t i = 0; i < 500000; i++) {
		insert_value(tree, (void *)i);
	}

	for (size_t i = 0; i < 500000; i++) {
		EXPECT_EQ((void *)i, find_node(tree, (void *)i)->value);
	}
}

TEST(rbtree, randomInsert) {
	rbtree *tree = create_rbtree();

	std::list<size_t> list;

	std::srand(1);
	std::uniform_int_distribution<> rand_int(0, 500'000'000);

	auto nil = get_mem_nil_value();

	for (size_t i = 0; i < 50'000; i++) {
		insert_value(tree, (void *)i);
		list.push_back(i);
	}

	for (auto elem : list) {
		EXPECT_EQ((void*)elem, find_node(tree, (void *)elem)->value);
		EXPECT_NE(nil, find_node(tree, (void *)elem));
	}
}
