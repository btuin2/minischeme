#include "alloc.h"

#include <cstring>
#include <gtest/gtest.h>
#include <vector>


TEST(alloc, simpleNumber) {

	double d = 3.1415;

	Gc::object_header* obj1 = alloc_object(&d, Gc::NUMBER, sizeof(d));

	EXPECT_EQ(obj1->magic_bytes, 0xdeadbeef);
	EXPECT_EQ(obj1->size, sizeof(d));
	EXPECT_EQ(obj1->type, Gc::NUMBER);

	EXPECT_EQ(get_value_number(obj1), 3.1415);

	Gc::object_header* obj2 = Gc::alloc_number(123456789.98765);

	EXPECT_EQ(obj2->magic_bytes, 0xdeadbeef);
	EXPECT_EQ(obj2->size, sizeof(d));
	EXPECT_EQ(obj2->type, Gc::NUMBER);

	EXPECT_EQ(get_value_number(obj2), 123456789.98765);

}


TEST(alloc, simpleString) {

	std::string s1 = "this is a string!!!";

	Gc::object_header* obj1 = alloc_object((void*) s1.c_str(), Gc::STRING, s1.size() + 1);

	EXPECT_EQ(obj1->magic_bytes, 0xdeadbeef);
	EXPECT_EQ(obj1->size, s1.size() + 1);
	EXPECT_EQ(obj1->type, Gc::STRING);

	int size1 = 0;
	const char * res1 = get_value_string(obj1, &size1);

	EXPECT_EQ(std::string(res1, size1), s1);
	EXPECT_EQ(size1, s1.size());


	std::string s2 = "another one with different value???";
	Gc::object_header * obj2 = Gc::alloc_string(s2.c_str(), s2.size());

	EXPECT_EQ(obj2->magic_bytes, 0xdeadbeef);
	EXPECT_EQ(obj2->size, s2.size() + 1);
	EXPECT_EQ(obj2->type, Gc::STRING);

	int size2 = 0;
	const char *res2 = get_value_string(obj2, &size2);

	EXPECT_EQ(std::string(res2, size2), s2);
	EXPECT_EQ(size2, s2.size());
}


TEST(alloc, list) {

	std::vector<Gc::object_header*> objs;

	objs.push_back(Gc::alloc_number(1));
	objs.push_back(Gc::alloc_number(2));
	objs.push_back(Gc::alloc_number(3));
	objs.push_back(Gc::alloc_number(4));
	objs.push_back(Gc::alloc_number(5));

	Gc::object_header* list = Gc::alloc_list(objs.data(), objs.size());

	EXPECT_EQ(list->magic_bytes, 0xdeadbeef);
	EXPECT_EQ(list->size, sizeof(Gc::list_element));
	EXPECT_EQ(list->type, Gc::LIST);

	Gc::object_header* res = list;

	EXPECT_EQ(get_value_number(get_list_data(res)), 1);
	res = Gc::get_list_next(res);
	EXPECT_EQ(get_value_number(get_list_data(res)), 2);
	res = Gc::get_list_next(res);
	EXPECT_EQ(get_value_number(get_list_data(res)), 3);
	res = Gc::get_list_next(res);
	EXPECT_EQ(get_value_number(get_list_data(res)), 4);
	res = Gc::get_list_next(res);
	EXPECT_EQ(get_value_number(get_list_data(res)), 5);

	res = Gc::get_list_next(res);
	EXPECT_EQ(res, nullptr);
}
